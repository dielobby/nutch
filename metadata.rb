name 'nutch'
maintainer 'Paul Ilea'
maintainer_email 'p.ilea@die-lobby.de'
license 'Apache 2.0'
description 'Installs/Configures Nutch'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.0.0'
recipe 'nutch::default', 'Main recipe'
issues_url 'https://bitbucket.org/dielobby/nutch/issues'
source_url 'https://bitbucket.org/dielobby/nutch'

depends 'ark'
